<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://tracker.port443.xyz/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://tracker.port443.xyz/favicon.ico" type="image/x-icon">
<meta name="Description" content="Port443.xyz Tracker Information">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdn-ovh.port443.xyz/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn-ovh.port443.xyz/jquery/jquery-3.3.1.min.js"></script>
<script src="https://cdn-ovh.port443.xyz/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Port443.xyz OpenTracker</title>
</head>
<body>
No torrents are hosted on this site. Please see https://en.wikipedia.org/wiki/BitTorrent_tracker for information on what a tracker does.
<br/><br/>
This tracker is intended for legal use only, please consult https://www.makeuseof.com/tag/8-legal-uses-for-bittorrent-youd-be-surprised/ for good use of the BitTorrent protocol.
<br/><br/>
This tracker supports torrent blacklisting. If you are the copyright owner for a torrent using this tracker and you want it blacklisted, please e-mail: <img src="cont.png">. Please note an official DMCA is not required, only proof you are the authorized representative or the copyright owner.
<br/><br/>
Blacklisting requests MUST include the infohash of the torrent. As mentioned, this is a tracker. It does not see file names and does not store any torrents. Without the infohash, we are unable to blacklist a torrent. Please refer to the above link on BitTorrent trackers for more information.
<br/><br/>
Tracker Announce Urls:
<br/><br/>
udp://tracker.port443.xyz:6969/announce
<br/>
http://tracker.port443.xyz:6969/announce
<br/><br/>
Please consider using UDP to help reduce server load.
<br/><br/>
This tracker supports IPv6.
<br/><br/>
<img src='/button-ipv6-80x15.png' alt='ipv6 ready' title='ipv6 ready' />
<br/><br/>
Please click <a href="https://tracker.port443.xyz/ðŸš«/">here</a> for a list of blacklisted torrents.
<br/><br/>
<p style="text-align:center;"><img src='/summary.png' alt='Data Summary' title='Data Summary'/>
<br/>
<img src='/hourly.png' alt='Data Hourly' title='Data Hourly'/>
<br/><br/>
<div class="container">
  <a href="#stats" class="btn btn-info" data-toggle="collapse">Stats</a>
  <div id="stats" class="collapse">
<?php
$torrentraw = "http://tracker.port443.xyz:6969/stats?mode=torr&format=txt";
$torrentnumber = file( $torrentraw );
$seedpeerraw = "http://tracker.port443.xyz:6969/stats";
$seedpeer = file( $seedpeerraw );
$connraw = "http://tracker.port443.xyz:6969/stats?mode=conn&format=txt";
$completedraw = "http://tracker.port443.xyz:6969/stats?mode=completed&format=txt";
$completed = file ( $completedraw );
$conn = file( $connraw );
$connectionscalc = $conn[0] / $conn[2];
echo "<p align='center'> Torrents:\r\n";
echo $torrentnumber[0]; //
echo "<br/>\r\n";
echo "Seeders:\r\n";
echo $seedpeer[1]; //
echo "<br/>\r\n";
echo "Peers:\r\n";
echo $seedpeer[0];
echo "<br/>\r\n";
echo "Completed:\r\n";
echo $completed[0];
echo "<br/>\r\n";
echo "Total Successful Connections (TCP + UDP):\r\n";
echo $conn[0]; //
echo "<br/>\r\n";
echo "Time Since Last Restart:\r\n";
echo $conn[2]; //
echo "<br/>\r\n";
echo "Connections Per Second:\r\n";
echo $connectionscalc; //
echo "<br/>\r\n";
echo "<br/>\r\n";
$now = new DateTime();
echo $now->format('D j F, Y H:i:s');
echo "<br/>\r\n";
echo "<br/>\r\n";
echo "Tracker stats are reset upon tracker restart."
?>
    </div>
  </div>
</body>
</html>
