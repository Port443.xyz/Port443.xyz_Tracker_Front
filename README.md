This requires any popular webserver with PHP 5+ installed along with VnStat and VnStati. OpenTracker is assumed to be running on both TCP and UDP on port 6969.

This will only work when using OpenTracker, in this site blacklisting is enabled. In cases where blacklisting is not enabled, simply remove the link to blacklisted torrents.

To start, you should have scripts/vnstati-hourly.sh and scripts/vnstati-summary.sh running. I use pm2 to run the tracker and the two scripts.

After ensuring scripts are running, edit index.php, changing the absolute links to your own web url, you may use the CDN if you like.

After this, replace cont.png with a screenshot of your e-mail address. This is to stop web spiders harvesting your e-mail from text.

To view this site in action, navigate to https://tracker.port443.xyz